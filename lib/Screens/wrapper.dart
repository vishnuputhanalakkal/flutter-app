import 'package:firebase_flutter/Modals/users.dart';
import 'package:firebase_flutter/Screens/Authenticate/authenticate.dart';
import 'package:firebase_flutter/Screens/Home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Users user = Provider.of<Users>(context);
    print(user.uid);
    if (user.uid != "") {
      return Home();
    } else {
      return Authenticate();
    }
  }
}
