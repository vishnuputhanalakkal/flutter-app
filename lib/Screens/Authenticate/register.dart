import 'package:firebase_flutter/Services/auth.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  final Function toggleAuthView;
  Register({required this.toggleAuthView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  String email = "";
  String password = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[100],
      appBar: AppBar(
        backgroundColor: Colors.green[300],
        centerTitle: true,
        title: Text("Sign Up"),
        elevation: 0.0,
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {
                widget.toggleAuthView();
              },
              icon: Icon(
                Icons.login,
                color: Colors.white,
              ),
              label: Text(
                "sign-in",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Email",
                  ),
                ),
                TextFormField(
                  onChanged: (value) {
                    setState(() {
                      email = value;
                    });
                  },
                  validator: (value) {
                    if (value != null && value.isEmpty) {
                      return "Enter a email id";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 20.0,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Password",
                  ),
                ),
                TextFormField(
                  obscureText: true,
                  onChanged: (value) {
                    setState(() {
                      password = value;
                    });
                  },
                  validator: (value) =>
                      value!.length < 6 ? "Enter password with 6+ char" : null,
                ),
                SizedBox(
                  height: 20.0,
                ),
                RaisedButton(
                    color: Colors.green[300],
                    child: Text(
                      "Sign UP",
                      style: TextStyle(color: Colors.grey[100]),
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        print(email);
                        print(password);
                      }
                    })
              ],
            ),
          )),
    );
  }
}
