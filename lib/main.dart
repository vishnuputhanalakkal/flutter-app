import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_flutter/Modals/users.dart';
import 'package:firebase_flutter/Screens/Authenticate/authenticate.dart';
import 'package:firebase_flutter/Screens/wrapper.dart';
import 'package:firebase_flutter/Services/auth.dart';
import 'package:firebase_flutter/firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// void main() {
//   runApp(const MyApp());
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<Users>.value(
      value: AuthService().user(),
      initialData: Users(),
      child: MaterialApp(
        home: Wrapper(),
      ),
      catchError: (context, error) {
        return Users();
      },
    );
  }
}
