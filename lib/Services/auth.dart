import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_flutter/Modals/users.dart';

class AuthService {
  FirebaseAuth _auth = FirebaseAuth.instance;

  // create user object
  dynamic _generateUser(user) {
    return user != null ? Users(uid: user.uid) : null;
  }

  Stream<Users> user() {
    return _auth.authStateChanges().map((user) => _generateUser(user));
  }

  // Sign in anon
  Future signInAnon() async {
    try {
      var result = await _auth.signInAnonymously();
      var user = result.user;
      return _generateUser(user);
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

  // Sign in with email/password

  // Register with email&password

  // Sign out

  Future signOut() async {
    try {
      print('going to lougout');
      await _auth.signOut();
    } catch (err) {
      print('error');
      print(err.toString());
      return null;
    }
  }
}
